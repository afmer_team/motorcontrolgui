import logging
import sys
import getopt

import serial

from pydominik import dominik_net0
from pydominik.Dominik import Dominik

request_mode = 0
scenario_mode = 1
mode = scenario_mode


def setup_test():
    # PARAMETER PARSING
    port_com = None
    argv = sys.argv[1:]

    try:
        opts, args = getopt.getopt(argv, "PORT:", ["PORT="])
    except getopt.GetoptError as err:
        print(err)  # will print something like "option -a not recognized"

    for opt, arg in opts:
        if opt in ["-PORT", "--PORT"]:
            port_com = arg

    test_object = Dominik(port_com)

    # USAGE
    test_object.req_break_step(0)
    test_object.req_result_per_step(0)
    test_object.req_clear_list()
    scenario_test(test_object)
    test_object.req_rotation_in_time(mode, 0.0, 20.0, 20)
    test_object.req_start_test()


def scenario_test(test_object):
    test_object.req_rotation_in_time(mode, 20.0, 0.6, 33340)
    test_object.req_rotation_in_time(mode, 9.0, 10.0, 1101)
    test_object.req_rotation_in_time(mode, -200.0, 12.0, 19170)
    test_object.req_rotation_in_time(mode, 1.0, 20.0, 95100)
    test_object.req_rotation_in_time(mode, 0.0, 20.0, 51)



setup_test()

