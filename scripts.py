import logging
import subprocess

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QGroupBox, QProgressBar, QLabel, QHBoxLayout, QTextEdit, \
    QFileDialog, QGridLayout, QMessageBox

from pydominik.Dominik import Dominik
from worker import Worker

log = logging.getLogger(__name__)


class scripts(QWidget):
    def __init__(self, parent):
        super(self.__class__, self).__init__()
        self.button_start_test = QPushButton("START TEST")
        self.button_clear_list = QPushButton("CZYŚĆ LISTĘ")
        self.button_break_step = QPushButton("PRZERWIJ KROK")
        self.button_test_status = QPushButton("ODCZYTAJ STATUS TESTU")
        self.button_break_test = QPushButton("PRZERWIJ TEST")
        self.text_path = QTextEdit(self)
        self.button_open = QPushButton('WYBIERZ PLIK')
        self.path = None
        self.time_of_test_arg = QLabel()
        self.time_of_test_remaining = QLabel()
        self.test_error = QLabel()
        self.progress_bar = QProgressBar(self)
        self.parent = parent
        self.set_connections()
        self.set_view()

    def set_view(self):
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.create_buttons())
        self.setLayout(main_layout)

    def enable_buttons(self, enable: bool):
        self.button_clear_list.setEnabled(enable)
        self.button_break_step.setEnabled(enable)
        self.button_test_status.setEnabled(False)
        self.button_open.setEnabled(enable)
        self.button_break_test.setEnabled(enable)
        self.button_start_test.setEnabled(False)

    def enable_buttons_scirpt(self):
        self.button_start_test.setEnabled(True)

    def set_connections(self):
        self.button_start_test.clicked.connect(self.send_test_list)
        self.button_clear_list.clicked.connect(self.clear_list)
        self.button_break_step.clicked.connect(self.break_step)
        self.button_test_status.clicked.connect(self.update_progress_bar)
        self.button_break_test.clicked.connect(self.break_test)

    def create_buttons(self):
        groupBox = QGroupBox("PANEL OBSŁUGI TESTU")
        gridbox = QGridLayout()

        gridbox.addWidget(self.button_open, 0, 0)
        gridbox.addWidget(self.text_path, 0, 1)
        gridbox.addWidget(self.button_test_status, 1, 0)
        gridbox.addWidget(self.progress_bar, 1, 1)

        self.text_path.setReadOnly(True)
        self.text_path.setText("WYBIERZ SKRYPT TESTOWY ABY DODAĆ JEGO SCIEŻKĘ")
        self.button_open.clicked.connect(self.open_text)

        gridbox.addWidget(self.time_of_test_remaining, 2, 1)
        gridbox.addWidget(self.time_of_test_arg, 3, 1)
        gridbox.addWidget(self.test_error, 4, 1)
        gridbox.addWidget(self.button_start_test, 2, 0)
        gridbox.addWidget(self.button_clear_list, 3, 0)
        gridbox.addWidget(self.button_break_step, 4, 0)
        gridbox.addWidget(self.button_break_test, 5, 0)

        gridbox.setAlignment(self.button_open, Qt.AlignTop)

        groupBox.setLayout(gridbox)
        self.show()
        return groupBox

    def update_progress_bar(self):
        logging.info("Update statusu testu")
        worker = Worker(self.parent.dominik.req_test_status)
        worker.signals.result.connect(self.status_bar_updated)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def status_bar_updated(self, result):
        logging.info("Updated")
        timestamp = result.get_value('timestamp')
        estimated_test_time = result.get_value('estimated_test_time')
        current_status = (timestamp / estimated_test_time) * 100
        self.time_of_test_remaining.setText("Pozostały czas trwania testu [s] : " + str(estimated_test_time - timestamp))
        self.progress_bar.setValue(current_status)

    def start_test(self):
        logging.info("Test startuje")
        worker = Worker(self.parent.dominik.req_start_test)
        worker.signals.result.connect(self.operation_passed)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def send_test_list(self):
        port_name = str(self.parent.com_port_name)
        self.parent.dominik.port.close()
        subprocess.call(["py", "{}".format(self.path), "--PORT", str(port_name)])
        self.parent.dominik = Dominik(port_name)
        self.start_test()
        self.button_start_test.setDisabled(True)
        self.button_test_status.setEnabled(True)

    def clear_list(self):
        logging.info("Czyszczenie listy")
        worker = Worker(self.parent.dominik.req_clear_list)
        worker.signals.result.connect(self.operation_passed)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def break_step(self):
        logging.info("Przerywanie kroku")
        worker = Worker(self.parent.dominik.req_break_step, flag=1)
        worker.signals.result.connect(self.operation_passed)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def break_test(self):
        logging.info("Przerywanie testu")
        worker = Worker(self.parent.dominik.req_break_step, flag=0)
        worker.signals.result.connect(self.operation_passed)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def operation_passed(self):
        logging.info("Operacja powiodła się")

    def operation_failed(self):
        logging.error("Operacja nieudana")

    def open_text(self):
        file_dialog = QFileDialog(self, "Wybierz plik skryptu",
                                  filter="(*.py)")
        file_dialog.setFileMode(QFileDialog.ExistingFile)

        if not file_dialog.exec():
            return

        self.path = file_dialog.selectedFiles()[0]

        self.text_path.setText(self.path)
        self.test_error.setText("")
        test = subprocess.Popen(["python", "{}".format(self.path), "--PORT", "TEST"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        data = test.stdout.readlines()
        print(data)
        error_data = test.stderr.readlines()

        if test.returncode is None:
            if len(data) or len(error_data) > 0:
                estimated_time = data[-1].decode()
                QMessageBox.information(self, "Błędy w teście", str(error_data))
                self.time_of_test_arg.setText("Szacowany czas trwania testu [s] :  " + str(estimated_time))
                if len(error_data) == 0 :
                    self.enable_buttons_scirpt()
        else:
            logging.info("Test posiada błędy")
            QMessageBox.information(self, "Błędy w teście", "Wgrany test posiada błędy, popraw je i załaduj test jeszcze raz.")


