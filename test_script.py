from pydominik.Dominik import Dominik
import time



d = Dominik("COM10")
d.set_characteristics("example_characteristics.json")
with d.steps("Jakas sobie rampa"):
    d.req_rotation_in_time(1, 2.0, 2.0, 4000)
    d.req_rotation_in_time(1, 0, 2.0, 2000)

v, r = d.read_volume_and_revs()
print("Volume {} revs {}".format(v, r))

d.reset_test()
d.req_rotation_in_time(1, 2.0, 2.0, 4000)
d.req_rotation_in_time(1, 0, 2.0, 2000)

d.req_start_test()

d.wait_for_test_to_finish()

v, r = d.read_volume_and_revs()
print("Volume {} revs {}".format(v, r))

