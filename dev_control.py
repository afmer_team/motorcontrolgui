import logging

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLineEdit, QVBoxLayout, QSizePolicy, QGridLayout, QSpacerItem, \
    QPushButton, QGroupBox, QSlider, QDoubleSpinBox, QLabel, QHBoxLayout

from double_slider import DoubleSlider
from worker import Worker

log = logging.getLogger(__name__)


class dev_control(QWidget):
    def __init__(self, parent):
        super(self.__class__, self).__init__()
        self.label_min = QLabel()
        self.label_maks = QLabel()
        self.rotation = QLabel()
        self.display_rotation = QDoubleSpinBox()
        self.vel_box1 = QDoubleSpinBox()
        self.button_send_velocity = QPushButton("WYŚLIJ SZYBKOŚĆ")
        self.clear_button = QPushButton("ZERUJ SZYBKOŚĆ")
        self.clear_button_velocity_degreebox = QPushButton("ZERUJ SZYBKOŚĆ")
        self.button_accept_degrees_degreebox = QPushButton("PRZESUŃ O KĄT")
        self.vel_box = QDoubleSpinBox()
        self.degree_box = QDoubleSpinBox()
        self.slider = DoubleSlider(1, Qt.Horizontal)
        self.button_clear_acc_vel = QPushButton("ZERUJ PRZYSPIESZENIE")
        self.button_clear_acc_deg = QPushButton("ZERUJ PRZYSPIESZENIE")
        self.acc_value_for_vel_box = QDoubleSpinBox()
        self.acc_value_box_degrees_box = QDoubleSpinBox()
        self.parent = parent
        self.degree_group_box = self.create_box_degrees()
        self.set_connections()
        self.set_view()

    def set_view(self):
        main_layout = QHBoxLayout()
        main_layout.addWidget(self.create_velocity_box())
        main_layout.addWidget(self.degree_group_box)

        self.setLayout(main_layout)

    def set_connections(self):
        self.acc_value_for_vel_box.setKeyboardTracking(False)
        self.vel_box.setKeyboardTracking(False)
        self.slider.sliderReleased.connect(self.send_req_rotation_for_slider)
        self.slider.valueChanged.connect(self.set_box_value)
        self.clear_button.clicked.connect(self.clear_slider_value_velocity_box)
        self.button_clear_acc_vel.clicked.connect(self.clear_acceleration_button_clicked_velocity_box)
        self.button_accept_degrees_degreebox.clicked.connect(self.set_degree_change)
        self.clear_button_velocity_degreebox.clicked.connect(self.clearing_velocity)
        self.button_clear_acc_deg.clicked.connect(self.clearing_acceleration)
        self.button_send_velocity.clicked.connect(self.send_req_rotation_for_box)
        self.display_rotation.valueChanged.connect(self.update_degree_box)
        self.degree_box.valueChanged.connect(self.update_rotation_display)

    def enable_buttons(self, enable: bool):
        self.clear_button.setEnabled(enable)
        self.button_clear_acc_vel.setEnabled(enable)
        self.slider.setEnabled(enable)
        self.button_send_velocity.setEnabled(enable)
        self.clear_button_velocity_degreebox.setEnabled(enable)
        self.button_accept_degrees_degreebox.setEnabled(enable)
        self.button_clear_acc_deg.setEnabled(enable)
        self.display_rotation.setEnabled(enable)

    def create_velocity_box(self):
        groupBox = QGroupBox("Sterowanie szybkością")

        v_box = QVBoxLayout()
        h_box = QHBoxLayout()

        self.label_maks.setText("50")
        self.label_min.setText("-50")
        self.label_maks.setAlignment(Qt.AlignRight)
        self.label_min.setAlignment(Qt.AlignLeft)

        h_box.addWidget(self.label_min)
        h_box.addWidget(self.label_maks)

        self.vel_box.setRange(-50, 50)
        v_box.addWidget(self.slider)
        v_box.addLayout(h_box)

        v_box.addWidget(self.vel_box)
        v_box.addWidget(self.button_send_velocity)
        v_box.addWidget(self.acc_value_for_vel_box)
        v_box.addWidget(self.button_clear_acc_vel)

        self.acc_value_for_vel_box.setValue(20)

        groupBox.setLayout(v_box)

        return groupBox

    def send_req_rotation_for_slider(self):
        log.info("Początek sterowania szybkością")
        worker = Worker(self.parent.dominik.req_rotation_in_time, flag=0x00, rotational_velocity=self.slider.value(),
                        rotational_acceleration=self.acc_value_for_vel_box.value(),
                        operation_time=0)
        worker.signals.result.connect(self.operation_passed)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def send_req_rotation_for_box(self):
        log.info("Początek sterowania szybkością")
        worker = Worker(self.parent.dominik.req_rotation_in_time, flag=0x00, rotational_velocity=self.vel_box.value(),
                        rotational_acceleration=self.acc_value_for_vel_box.value(),
                        operation_time=0)
        worker.signals.result.connect(self.operation_passed)
        worker.signals.error.connect(self.operation_failed)
        self.parent.dominik_threadpool.start(worker)

    def set_box_value(self):
        self.slider.setIntValue()
        if self.slider.value() != 0:
            self.degree_group_box.setDisabled(True)
        else:
            self.degree_group_box.setDisabled(False)
        if self.slider.value() > 30 or self.slider.value() < -30:
            self.acc_value_for_vel_box.setRange(0, 20)
        else:
            pass

        self.vel_box.setValue(self.slider.value())

    def clear_acceleration_button_clicked_velocity_box(self):
        self.acc_value_for_vel_box.setValue(0)

    def clear_slider_value_velocity_box(self):
        self.slider.setValue(0)

    def set_slider_velocity_box(self):
        self.slider.setValue(self.vel_box.value())

    def operation_passed(self, result):
        logging.info("Sterowanie szybkością działa")

    def operation_failed(self):
        logging.error("Sterowanie szybkością nie działa")

    def create_box_degrees(self):
        groupBox = QGroupBox("Sterowanie przesunięciem kątowym")
        self.rotation.setText("OBROTY")

        vbox = QVBoxLayout()
        hbox = QHBoxLayout()

        hbox.addWidget(self.rotation)
        hbox.addWidget(self.display_rotation)
        self.display_rotation.setRange(-32000, 32000)

        self.vel_box1.setRange(0, 50)
        self.degree_box.setRange(-32000,32000)

        vbox.addLayout(hbox)
        vbox.addWidget(self.degree_box)
        vbox.addWidget(self.button_accept_degrees_degreebox)

        vbox.addWidget(self.vel_box1)
        vbox.addWidget(self.clear_button_velocity_degreebox)

        vbox.addWidget(self.acc_value_box_degrees_box)
        vbox.addWidget(self.button_clear_acc_deg)

        groupBox.setLayout(vbox)

        self.acc_value_box_degrees_box.setValue(1)
        self.degree_box.setKeyboardTracking(False)
        self.vel_box1.setKeyboardTracking(False)

        return groupBox

    def clearing_velocity(self):
        self.vel_box1.setValue(0)

    def clearing_acceleration(self):
        self.acc_value_box_degrees_box.setValue(0)

    def set_degree_change(self):
        worker = Worker(self.parent.dominik.req_rotation_degrees, flag=0x00,
                        rotation_in_degrees = int(self.degree_box.value()), rotational_velocity=self.vel_box1.value(),
                        rotational_acceleration=self.acc_value_box_degrees_box.value())
        worker.signals.result.connect(self.set_degree_change_passed)
        worker.signals.error.connect(self.set_degree_change_failed)

        self.parent.dominik_threadpool.start(worker)

    def set_degree_change_passed(self):
        log.info("Przesunięcie o kąt udane")

    def set_degree_change_failed(self):
        log.error("Przesunięcie o kąt nieudane")

    def update_degree_box(self):
        self.degree_box.setValue(self.display_rotation.value()*360)

    def update_rotation_display(self):
        self.display_rotation.setValue(self.degree_box.value()/360)
