from PyQt5.QtWidgets import QStatusBar
from PyQt5.QtCore import QTimer, Qt
from datetime import datetime


class StatusBar(QStatusBar):
    def __init__(self):
        super().__init__()
        self.com_port = ""
        self.timer = QTimer()

        self.update()
        self.timer.timeout.connect(self.update)
        self.timer.start()

    def update(self):
        local_time_str = datetime.now().strftime("%H:%M:%S %d.%m.%Y")
        utc_time_str = datetime.utcnow().strftime("%H:%M:%S %d.%m.%Y")
        self.showMessage("COM: {}  |  Czas lokalny {}   |   UTC {}".format( \
            self.com_port, \
            local_time_str, \
            utc_time_str))

    def update_com(self, com_port):
        self.com_port = com_port
        self.update()
