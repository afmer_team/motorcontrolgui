import os
import sys

import qdarkstyle
import serial
import serial.tools.list_ports
from PyQt5 import QtWidgets
from PyQt5.QtCore import QThreadPool
from PyQt5.QtWidgets import QMainWindow, QWidget, QAction, QTabWidget, QVBoxLayout, QGroupBox, QTableWidgetItem, \
    QSizePolicy, QTableWidget, QHBoxLayout
from PyQt5.QtWidgets import QPushButton

from pydominik.Dominik import Dominik
from dev_control import dev_control
from scripts import scripts
from status import StatusBar
from volume import volume
from worker import Worker
import logging

if not os.path.isdir("logs"):
    os.mkdir("logs")

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s:%(levelname)s:%(module)s:%(message)s",
                    handlers=[logging.FileHandler(os.path.join("logs", "logging.log")),
                              logging.StreamHandler()])


class GUI(QMainWindow):
    def __init__(self):
        super(GUI, self).__init__()
        logging.info("----------")
        logging.info("Start Dominik Viewer")
        self.setWindowTitle("Dominik Viewer")
        self.com_port_name = ""
        self.dominik = None
        self.dominik_threadpool = QThreadPool()
        self.tabs_widget = QTabWidget()
        self.volume_tab = volume(parent=self)
        self.scripts_tab = scripts(parent=self)
        self.control_tab = dev_control(parent=self)
        self.status_bar = StatusBar()
        self.button_read = QPushButton("ODCZYTAJ/AKTUALIZUJ")
        self.button_clear = QPushButton("ZERUJ")
        self.table = QTableWidget(3, 2)
        self.set_menu_bar()
        self.set_resizing()
        self.set_view()
        self.set_connections()
        self.enable_all_buttons(False)
        self.enable_read_clear_box(False)

    def enable_read_clear_box(self, enable):
        self.button_clear.setEnabled(enable)
        self.button_read.setEnabled(enable)

    def enable_all_buttons(self, enable):
        for i in range(self.tabs_widget.count()):
            self.tabs_widget.widget(i).enable_buttons(enable)

    def set_resizing(self):
        self.volume_tab.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.scripts_tab.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.control_tab.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.tabs_widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def set_connections(self):
        self.serial_menu.aboutToShow.connect(self.update_serial_ports)
        self.serial_menu.triggered.connect(self.com_port_selected)
        self.button_read.clicked.connect(self.read_general_data)
        self.button_clear.clicked.connect(self.clear_general_data)

    def set_view(self):
        self.setWindowTitle("DOMINIK 2.0 Viewer")
        main_layout = QVBoxLayout()

        main_layout.addWidget(self.tabs_widget)
        self.tabs_widget.addTab(self.control_tab, "Tryb deweloperski")
        self.tabs_widget.addTab(self.scripts_tab, "Tryb scenariuszy")
        self.tabs_widget.addTab(self.volume_tab, "Objętość")
        main_layout.addWidget(self.create_read_clear_box())

        self.setStatusBar(self.status_bar)
        self.setCentralWidget(QWidget())
        self.centralWidget().setLayout(main_layout)
        self.setFixedSize(main_layout.sizeHint())

    def set_menu_bar(self):
        menu_bar = self.menuBar()
        settings_menu = menu_bar.addMenu("Ustawienia")
        self.serial_menu = settings_menu.addMenu("Port szeregowy")

    def show_port(self):
        pass

    def create_read_clear_box(self):
        groupBox = QGroupBox("ODCZYTAJ/ZERUJ")
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()

        vbox.addWidget(self.button_read)
        vbox.addWidget(self.button_clear)
        vbox.addWidget(self.button_clear)
        vbox.addWidget(self.button_clear)
        vbox.addWidget(self.button_clear)

        hbox.addLayout(vbox)
        hbox.addWidget(self.table)

        self.table.setHorizontalHeaderLabels(["WIELKOŚĆ", "WARTOŚĆ"])
        current_rotation_velocity = QTableWidgetItem("SZYBKOŚĆ: ")
        rotation_counter = QTableWidgetItem("OBROTY:")
        timestamp = QTableWidgetItem("TIMESTAMP:")
        self.table.setItem(0, 0, current_rotation_velocity)
        self.table.setItem(1, 0, rotation_counter)
        self.table.setItem(2, 0, timestamp)
        self.table.setEnabled(False)

        groupBox.setLayout(hbox)
        self.show()

        return groupBox

    def clear_general_data(self):
        worker = Worker(self.dominik.req_clear_general_data)
        worker.signals.result.connect(self.data_cleared)
        worker.signals.error.connect(self.data_not_cleared)

        self.dominik_threadpool.start(worker)

    def data_cleared(self):
        logging.info("Dane wyczyszczone")

    def data_not_cleared(self):
        logging.error("Błąd czyszczenia danych")

    def read_general_data(self):
        worker = Worker(self.dominik.req_general_data)
        worker.signals.result.connect(self.data_aquiring)
        worker.signals.error.connect(self.data_not_aquiring)

        self.dominik_threadpool.start(worker)

    def data_aquiring(self, result):
        logging.info("Dane są przyjmowane")
        print(result)
        print(result.get_bytes())
        current_rotation_velocity = round(result.get_value('current_rotation_velocity'), 2)
        rotation_counter = result.get_value('rotation_counter')
        timestamp = result.get_value('timestamp')
        self.table.setItem(0, 1, QTableWidgetItem(str(current_rotation_velocity)))
        self.table.setItem(0, 3, QTableWidgetItem(str(rotation_counter / 10.0)))
        self.table.setItem(0, 5, QTableWidgetItem(str(timestamp)))
        self.table.resizeColumnsToContents()

    def data_not_aquiring(self):
        logging.error("Błąd odczytu danych")

    def open_new_port(self):
        if self.com_port_name == "":
            logging.info("Wylaczam port szeregowy")
            self.dominik_threadpool.waitForDone(msecs=2000)
            self.dominik = None
            self.status_bar.update_com(self.com_port_name)
            self.enable_all_buttons(False)
            self.enable_read_clear_box(False)
            return

        try:
            logging.info("Otwieram port {}".format(self.com_port_name))
            self.dominik = Dominik(self.com_port_name)
            self.status_bar.update_com(self.com_port_name)
            self.enable_all_buttons(True)
            self.enable_read_clear_box(True)
        except serial.SerialException:
            logging.error("Nie udalo sie otworzyc portu")
            self.com_port_name = ""

    def com_port_selected(self, action):
        new_port_name = action.text()

        if new_port_name == self.com_port_name:
            self.com_port_name = ""
        else:
            self.com_port_name = action.text()

        self.open_new_port()

    def update_serial_ports(self):
        self.serial_menu.clear()

        for el in serial.tools.list_ports.comports():
            port_action = QAction(el.device, self)
            port_action.setCheckable(True)
            if el.device == self.com_port_name:
                port_action.setChecked(True)
            self.serial_menu.addAction(port_action)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    mainWin = GUI()
    mainWin.show()
    sys.exit(app.exec_())
