import struct
import time
import serial

STX = 0x02
ETX = 0x03


def crc16(buff: bytearray):
    result = 0xFFFF
    for bajt in buff:
        result ^= bajt
        result &= 0xFFFF
        for bit in range(8, 0, -1):
            if (result & 0x0001) != 0:
                result = result >> 1
                result = result ^ 0xA001
            else:
                result = result >> 1
            result &= 0xFFFF
    return result


def pack(data):
    crc = crc16(data)
    data += bytearray([(crc & 0xFF00) >> 8, crc & 0x00FF])
    out_frame = bytearray([0x02])
    for b in data:
        if b == 0x02 or b == 0x03 or b == 0x10:
            out_frame.append(0x10)
            b |= 0x80
        out_frame.append(b)
    out_frame.append(0x03)
    return out_frame


def unpack(frame: bytearray):
    unpacked = bytearray()
    if frame[0] != 0x02 or frame[-1] != 0x03:
        return None
    iframe = iter(frame[1:-1])
    for b in iframe:
        if b == 0x10:
            unpacked.append(next(iframe, None) & 0x7F)
        else:
            unpacked.append(b)
    if crc16(unpacked[:-2]) == struct.unpack(">H", unpacked[-2:])[0]:
        return unpacked[:-2]
    else:
        raise ValueError("CRC Error")
