from multipledispatch import dispatch
import struct
import binascii
from collections import OrderedDict
__pack = {
            'uint8_t': {'Little': '<B', 'Big': '>B', 'size': 1},
            'int8_t': {'Little': '<b', 'Big': '>b', 'size': 1},
            'uint16_t': {'Little': '<H', 'Big': '>H', 'size': 2},
            'int16_t': {'Little': '<h', 'Big': '>h', 'size': 2},
            'uint32_t': {'Little': '<I', 'Big': '>I', 'size': 4},
            'int32_t': {'Little': '<i', 'Big': '>i', 'size': 4},
            'float': {'Little': '<f', 'Big': '>f', 'size': 4},
            'flexible': {'size': None}
         }


class Cstruct:

    def __init__(self, frame, endian='Little'):
        # czy flexible jest na koncu struktury
        if type(frame) is list:
            self.frame = OrderedDict()
            for i, el in enumerate(frame):
                k = list(el.keys())[0]
                v = el[k]
                if v['type'] == "flexible" and i < (len(frame) - 1):
                    raise ValueError("Flexible item not at the end of structure")
                self.frame[k] = v
        else:
            self.frame = frame
        self.endian = endian

    def __str__(self):
        result = ''
        for key, record in self.frame.items():
            try:
                if type(record['value']) is list:
                    result += key + ':\n'
                    for row in record['value']:
                        result += '\t' + row['name'] + ': ' + hex(row['value']) + '\n'
                if type(record['value']) is bytes:
                    result += key + ': ' + hex_string(record['value']) + '\n'
                elif type(record['value'] is float):
                    result += key + ': ' + str(record['value']) + '\n'
                else:
                    result += key + ': ' + hex(record['value']) + '\n'
            except KeyError:
                result += key + ': None\n'
        return result[:-1]


    def set_values(self, values):
        for value in values:
            self.set_value(value[0], value[1])


    def set_value(self, key, value):
        try:
            self.frame[key]['value'] = value
        except KeyError:
            print("Error: key not found in cstruct")
            raise ValueError


    def get_value(self, key):
        try:
            return self.frame[key]['value']
        except KeyError:
            print("Error: key not found in cstruct")
            raise ValueError


    def get_size(self):
        return _sizeof(self.frame)


    def get_stream(self):
        return _prepare_bytes(self.frame, self.endian)


    def get_bytes(self):
        return _convert_stream_to_bytes(self.get_stream())

    @dispatch(list)
    def parse(self, stream):
        self.frame = _prepare_values(_convert_bytes_to_stream(stream), \
                                     self.frame, self.endian)

    @dispatch(bytes)
    def parse(self, stream):
        self.frame = _prepare_values(stream, self.frame, self.endian)


def _convert_bytes_to_stream(frame):
    result = b''
    for byte in frame:
        result += struct.pack('<B', byte)
    return result


def _convert_stream_to_bytes(stream):
    return [val for val in stream]


def _prepare_bytes(cstruct, endianness):
    result = b''
    for key, record in cstruct.items():
        result += _prepare_record(record, endianness)
    return result


def _prepare_record(record, endianness):
    record_type = record['type'].split(':')
    record_value = record['value']
    if len(record_type) == 2 and record_type[1] == 'flags':
        result = 0
        for flag in record['value']:
            result |= flag['value'] << flag['position']
        record_value = result
    try:
        return struct.pack(__pack[record_type[0]][endianness], record_value)
    except ValueError:
        print("Invalid structure")


def _prepare_values(stream, cstruct, endianness):
    result = OrderedDict()
    i = 0
    for key, record in cstruct.items():
        record_type = record['type'].split(':')
        if record_type[0] == 'flexible':
            result[key] = {'value': stream[i:], 'type': 'flexible'}
            return result

        try:
            result[key] = {'value': \
                                struct.unpack(__pack[record_type[0]][endianness], \
                                stream[i:i+__pack[record_type[0]]['size']])[0], \
                           'type': \
                                record['type']}
        except ValueError:
            print("Invalid structure")
        i += __pack[record_type[0]]['size']
        if len(record_type) == 2 and record_type[1] == 'flags':
            value = result[key]['value']
            values = []
            for flag in record['value']:
                values.append({'name': flag['name'], 'value': \
                    (value >> flag['position']) & (2**flag['size']-1)})
            result[key]['value'] = values[:]
    return result

    def __bytes__(self):
        return self.get_bytes()

def _sizeof(cstruct):
    result = 0
    for key, record in cstruct.items():
        record_type = record['type'].split(':')[0]
        result += __pack[record_type]['size']
    return result

def hex_string(data):
    if data:
        return ' '.join([hex(b)[2:].zfill(2).upper() for b in data])
    else:
        return "NULL"
