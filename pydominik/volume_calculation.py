import pickle
from itertools import tee
from math import fabs
import json
import jsonschema


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def volume_of_max_time(ch):
    return ch["points"][0][1]


def time_exceeds_char_max(ch, rev_time):
    return rev_time > ch["points"][0][0]


def time_exceeds_char_min(ch, rev_time):
    return rev_time < ch["points"][-1][0]


def volume_of_min_time(ch):
    return ch["points"][-1][1]


def interpolate(x0, y0, x1, y1, x):
    denominator = x1 - x0
    if denominator != 0.0:
        a = (y1 - y0) / denominator
        b = y0 - a * x0
        return a * x + b
    else:
        return 0.0


def find_surrounding_points(ch, rev_time):
    for p1, p2 in pairwise(ch["points"]):
        if rev_time > p2[0]:
            return p1, p2
    return None, None


def single_char_interpolation(ch, rev_time):
    l_p, u_p = find_surrounding_points(ch, rev_time)

    return interpolate(u_p[0], u_p[1],
                       l_p[0], l_p[1],
                       rev_time)


def estimate_with_single_char(ch, rev_time):
    if (len(ch["points"]) == 1
            or time_exceeds_char_max(ch, rev_time)):
        return volume_of_max_time(ch)
    elif time_exceeds_char_min(ch, rev_time):
        return volume_of_min_time(ch)
    else:
        return single_char_interpolation(ch, rev_time)


def estimate_with_both_chars(lower_char, upper_char, rev_time, temperature):
    lower_char_estimation = estimate_with_single_char(lower_char, rev_time)
    upper_char_estimation = estimate_with_single_char(upper_char, rev_time)

    return interpolate(lower_char["temperature"], lower_char_estimation,
                       upper_char["temperature"], upper_char_estimation,
                       temperature)


def calc_volume(revolutions, revolution_time, temperature, orientation, characteristics):
    direction = 'p' if revolutions >= 0 else 't'

    # 1 wybrac charakterystyke
    lower_char, upper_char = select_characteristics_pair(temperature, orientation,
                                                         direction, characteristics)
    if lower_char is None and upper_char is None:
        return 0.0

    # 2 interpolowac punkty wewnatrz charakterystyk
    if lower_char is None:
        return revolutions * estimate_with_single_char(upper_char, revolution_time)
    elif upper_char is None:
        return revolutions * estimate_with_single_char(lower_char, revolution_time)
    elif temperature == lower_char["temperature"]:
        return revolutions * estimate_with_single_char(lower_char, revolution_time)
    else:
        return revolutions * estimate_with_both_chars(lower_char, upper_char, revolution_time, temperature)


def time_slicer(t, const_period):
    while t > const_period:
        yield const_period
        t -= const_period
    yield t


def ramp_slice(step, prev_step, temperature, orientation, characteristics):
    init_speed = prev_step["velocity"]
    final_speed = step["velocity"]
    ramp_time = fabs((final_speed - init_speed) / step["acceleration"])

    ramp_revs = 0
    ramp_volume = 0
    prev_speed = prev_step["velocity"]

    acc_sign = 1.0 if step["velocity"] - prev_step["velocity"] >= 0 else -1.0
    for slice_time in time_slicer(ramp_time, 1.0):
        current_speed = prev_speed + acc_sign * step["acceleration"] * slice_time
        avg_speed = (prev_speed + current_speed) / 2
        rev_time = fabs(1.0 / avg_speed)
        slice_revs = avg_speed * slice_time

        ramp_revs += slice_revs
        ramp_volume += calc_volume(slice_revs, rev_time, temperature, orientation, characteristics)
        prev_speed = current_speed
    return ramp_revs, ramp_volume


def volume_of_steps(steps, temperature, orientation, characteristics):
    if len(steps) == 0:
        return 0.0
    total_volume = 0.0
    zero_step = {'velocity': 0.0, 'acceleration': 0.0, 'amount_of_rotation': 0}
    steps.insert(0, zero_step)

    for prev_step, step in pairwise(steps):
        if step["velocity"] == prev_step["velocity"] and step["velocity"] != 0.0:
            revolutions = step["amount_of_rotation"]
            revolution_time = fabs(1.0 / step["velocity"])
            total_volume += calc_volume(revolutions=revolutions, revolution_time=revolution_time,
                                        temperature=temperature, orientation=orientation,
                                        characteristics=characteristics)
        else:
            ramp_revs, ramp_volume = ramp_slice(step, prev_step, temperature, orientation, characteristics)
            total_volume += ramp_volume

            if step["velocity"] != 0.0:
                const_revs = step["amount_of_rotation"] - ramp_revs
                const_revs_time = 1.0 / step["velocity"]
                const_vol = calc_volume(const_revs, const_revs_time, temperature, orientation, characteristics)
                total_volume += const_vol
    return total_volume


def revolutions_of_steps(steps):
    return sum([step["amount_of_rotation"] for step in steps])


def parse_characteristics_file(filename):
    schema = {"type": "array",
              "items":
                  {"type": "object",
                   "properties":
                       {
                           "num": {"type": "number"},
                           "direction": {"type": "string", "enum": ['p', 't']},
                           "orientation": {"type": "string", "enum": ['v', 'h']},
                           "temperature": {"type": "number"},
                           "points": {"type": "array", "items":
                               {"type": "array",
                                "items": [{"type": "number"},
                                          {"type": "number"}]
                                },
                                      "maxItems": 20
                                      }
                       },
                   "required": ["num", "direction", "orientation", "temperature", "points"],
                   }
              }
    with open(filename, 'r') as f:
        parsed_json = json.load(f)
        jsonschema.validate(instance=parsed_json, schema=schema)
        return parsed_json


def select_characteristics_pair(temperature, orientation, direction, characteristics):
    lower_char = None
    upper_char = None
    lower_temp = 0
    upper_temp = 0xFF

    for char in filter(lambda c: c["direction"] == direction and
                                 c["orientation"] == orientation,
                       characteristics):
        if temperature <= char["temperature"] <= upper_temp:
            upper_temp = char["temperature"]
            upper_char = char
        if temperature >= char["temperature"] >= lower_temp:
            lower_temp = char["temperature"]
            lower_char = char

    return lower_char, upper_char


if __name__ == "__main__":
    characteristics = parse_characteristics_file("example_characteristics.json")
    temperature = 13
    orientation = 'h'

    with open("data2", 'rb') as f:
        steps = pickle.load(f)

    print("V = {} [L]".format(
        volume_of_steps(steps, temperature=temperature, orientation=orientation, characteristics=characteristics)))
    print("Obroty = {}".format(revolutions_of_steps(steps)))
##
