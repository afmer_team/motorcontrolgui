from pydominik import dominik_net0, net0
from pydominik.dominik_frames import REQ_BASE_POSITION, REQ_ROTATION_IN_TIME, DominikProtocolCmds, \
    REQ_ROTATION_DEGREES, REQ_GENERAL_DATA, REQ_CLEAR_GENERAL_DATA, REQ_BREAK_STEP, REQ_CLEAR_LIST, REQ_START_TEST, \
    REQ_RESULT_PER_STEP, REQ_TEST_STATUS, RESP_GENERAL_DATA
import os
import logging
from math import fabs

max_velocity = 50.0
treshold_velocity = 30.0
max_acceleration = 20.0

if not os.path.isdir("logs"):
    os.mkdir("logs")

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s:%(levelname)s:%(module)s:%(message)s",
                    handlers=[logging.FileHandler(os.path.join("logs", "emulator.log")),
                              logging.StreamHandler()])

class DominikEmulator:
    def __init__(self):
        self.cmds_list = []
        self.test_estimated_time = 0

    def test_req_start_test(self):
        logging.info("test_req_start_test passed")

    def test_req_clear_list(self):
        logging.info("test_req_clear_list passed")

    def test_req_break_step(self, flag):
        if (flag == 0 or flag == 1) and isinstance(flag, int):
            data_frame = REQ_BREAK_STEP()
            data_frame.set_value("flag", flag)
            DominikEmulator.check_data_valid(DominikProtocolCmds.REQ_BREAK_STEP, data_frame)
            logging.info("test_req_break_step passed")
        else:
            raise ValueError("Flag error at req_break_step")

    def test_req_rotation_in_time(self, flag, rotational_velocity, rotational_acceleration, operation_time):
        self.cmds_list.append(
            {"fn": "test_req_rotation_in_time", "flag": flag, "rotational_velocity": rotational_velocity,
             "rotational_acceleration": rotational_acceleration, "operation_time": operation_time})
        self.order_check()
        self.check_time_rules()
        self.sum_estimated_time()

        if (flag == 0 or flag == 1) and isinstance(flag, int):
            vars_to_check = [(rotational_velocity, float),
                             (rotational_acceleration, float),
                             (operation_time, int)]

            for var, var_type in vars_to_check:
                if type(var) is not var_type:
                    logging.info("expected {} to be {}, was {}".format(var, var_type, type(var)))
                    raise ValueError("Data valid test_req_rotation_in_time not passed")

            if fabs(rotational_velocity) > max_velocity:
                raise ValueError("Rotational valocity out of range")
            if fabs(rotational_velocity) > treshold_velocity and rotational_acceleration > max_acceleration:
                raise ValueError("Rotational acceleration out of range for given velocity")

            data_frame = REQ_ROTATION_IN_TIME()
            data_frame.set_value("flag", flag)
            data_frame.set_value("rotational_velocity", rotational_velocity)
            data_frame.set_value("rotational_acceleration", rotational_acceleration)
            data_frame.set_value("operation_time", operation_time)

            DominikEmulator.check_data_valid(DominikProtocolCmds.REQ_ROTATION_IN_TIME, data_frame)
            logging.info("test_req_rotation_in_time passed")

        else:
            raise ValueError("Flag error at req_rotation_in_time")

    def test_req_rotation_degrees(self, flag, rotation_in_degrees, rotational_velocity, rotational_acceleration):
        self.cmds_list.append(
            {"fn": "test_req_rotation_degrees", "flag": flag, "rotation_in_degrees": rotation_in_degrees,
             "rotational_velocity": rotational_velocity, "rotational_acceleration": rotational_acceleration})
        self.order_check()
        self.sum_estimated_time()

        if (flag == 0 or flag == 1) and isinstance(flag, int):
            vars_to_check = [(rotation_in_degrees, int),
                             (rotational_velocity, float),
                             (rotational_acceleration, float)]

            for var, var_type in vars_to_check:
                if type(var) is not var_type:
                    logging.info("expected {} to be {}, was {}".format(var, var_type, type(var)))
                    raise ValueError("Data valid test_req_rotation_degrees not passed")

            if rotational_velocity > max_velocity:
                raise ValueError("Rotational valocity out of range")
            if rotational_velocity > treshold_velocity and rotational_acceleration > max_acceleration:
                raise ValueError("Rotational acceleration out of range for given velocity")

            data_frame = REQ_ROTATION_DEGREES()
            data_frame.set_value("flag", flag)
            data_frame.set_value("rotation_in_degrees", rotation_in_degrees)
            data_frame.set_value("rotational_velocity", rotational_velocity)
            data_frame.set_value("rotational_acceleration", rotational_acceleration)

            DominikEmulator.check_data_valid(DominikProtocolCmds.REQ_ROTATION_DEGREES, data_frame)
            logging.info("test_req_rotation_degrees passed")

        else:
            raise ValueError("Flag error at req_rotation_degrees")

    def test_req_base_position(self, flag):
        if (flag == 0 or flag == 1) and isinstance(flag, int):
            data_frame = REQ_BASE_POSITION()
            data_frame.set_value("flag", flag)

            DominikEmulator.check_data_valid(DominikProtocolCmds.REQ_BASE_POSITION, data_frame)
            logging.info("test_req_base_position passed")

        else:
            raise ValueError("Flag error at req_base_position")

    def test_req_result_per_step(self, step_number):
        if isinstance(step_number, int) and step_number >= 0:
            data_frame = REQ_RESULT_PER_STEP()
            data_frame.set_value("step_number", step_number)

            DominikEmulator.check_data_valid(DominikProtocolCmds.REQ_RESULT_PER_STEP, data_frame)
            logging.info("test_req_result_per_step passed")

        else:
            raise ValueError("step_number error at req_base_position")

    def test_req_test_status(self):
        logging.info("test_req_test_status passed")

    def test_req_general_data(self):
        logging.info("test_req_general_data passed")

    def test_req_clear_general_data(self):
        logging.info("test_req_clear_general_data passed")

    def get_tester_req_from_req(self, function):
        tester_req = {
            "req_start_test": self.test_req_start_test,
            "req_clear_list": self.test_req_clear_list,
            "req_break_step": self.test_req_break_step,
            "req_rotation_in_time": self.test_req_rotation_in_time,
            "req_rotation_degrees": self.test_req_rotation_degrees,
            "req_base_position": self.test_req_base_position,
            "req_result_per_step": self.test_req_result_per_step,
            "req_test_status": self.test_req_test_status,
            "req_general_data": self.test_req_general_data,
            "req_clear_general_data": self.test_req_clear_general_data
        }
        try:
            return tester_req[function]
        except KeyError:
            logging.error("Nieznana komenda %d".format(function))
            return None

    @staticmethod
    def check_data_valid(cmd, data):
        data = data.get_bytes()
        if type(cmd) is DominikProtocolCmds:
            cmd = cmd.value
        raw_frame = [cmd] + data
        packed_frame = net0.pack(raw_frame)
        return packed_frame

    def check_time_rules(self):
        if len(self.cmds_list) == 1:
            if self.cmds_list[0]["fn"] == "test_req_rotation_in_time":
                time = (self.cmds_list[0]["rotational_velocity"] / (self.cmds_list[0]["rotational_acceleration"]) * 1000)
                if time >= (self.cmds_list[0]["operation_time"]):
                    raise ValueError("Too short time for proper ramp time in command:", self.cmds_list[0],
                                     "required at least:", time + 1)

        if len(self.cmds_list) >= 2:
            if self.cmds_list[-1]["fn"] == "test_req_rotation_in_time" and self.cmds_list[-2][
                "fn"] == "test_req_rotation_in_time":
                time = (fabs((self.cmds_list[-2]["rotational_velocity"] - self.cmds_list[-1]["rotational_velocity"]) /
                             self.cmds_list[-1]["rotational_acceleration"])) * 1000

                if time >= self.cmds_list[-1]["operation_time"]:
                    raise ValueError("Too short time for proper ramp time in command:", self.cmds_list[-1],
                                 "required at least:", time + 1)

        if self.cmds_list[-1]["fn"] == "test_req_rotation_in_time":
            if self.cmds_list[-1]["operation_time"] == 0:
                raise ValueError("Time has to be greater than 0 in command:", self.cmds_list[-1])

    def order_check(self):
        if len(self.cmds_list) >= 2:  # out of range safety
            if self.cmds_list[-1]["fn"] == "test_req_rotation_degrees" and self.cmds_list[-2][
                "fn"] == "test_req_rotation_in_time" and self.cmds_list[-2]["rotational_velocity"] != 0:
                raise ValueError("Cannot use req_rotation_degrees while tester is moving")

        if len(self.cmds_list) >= 3:  # out of range safety
            if self.cmds_list[-1]["fn"] == "test_req_rotation_degrees" and self.cmds_list[-2][
                "fn"] == "test_req_rotation_in_time" and self.cmds_list[-2]["rotational_velocity"] == 0 and \
                    self.cmds_list[-3]["fn"] == "test_req_rotation_in_time":
                time = self.cmds_list[-2]["operation_time"]
                previous_vel = self.cmds_list[-3]["rotational_velocity"]
                actual_acc = self.cmds_list[-2]["rotational_acceleration"]
                req_time = previous_vel / actual_acc
                if time < req_time:
                    raise ValueError("Tester needs more time to adjust velocity to zero")

    def sum_estimated_time(self):
        if len(self.cmds_list) >= 1:  # out of range safety
            if self.cmds_list[-1]["fn"] == "test_req_rotation_degrees":
                rotate_from_angles = fabs(self.cmds_list[-1]["rotation_in_degrees"] / 360)  # rotation_in_degrees/360
                if self.cmds_list[-1]["rotational_acceleration"] != 0 and self.cmds_list[-1]["rotational_velocity"]:
                    time1 = 2 * self.cmds_list[-1]["rotational_velocity"] / self.cmds_list[-1][
                        "rotational_acceleration"]
                    rotate1 = self.cmds_list[-1]["rotational_acceleration"] * time1 / 2
                    rotate2 = rotate_from_angles - rotate1
                    time2 = rotate2 / self.cmds_list[-1]["rotational_velocity"]
                    self.test_estimated_time += (time2 + time1)
                print(round(self.test_estimated_time / 1000))  # Print do obsługi przekazania czasu/nie usuwać
            elif self.cmds_list[-1]["fn"] == "test_req_rotation_in_time":
                self.test_estimated_time += self.cmds_list[-1]["operation_time"]
                print(round(self.test_estimated_time / 1000))  # Print do obsługi przekazania czasu/nie usuwać
            else:
                return 0
        else:
            return 0

