from pydominik import dominik_net0, net0
from pydominik.dominik_frames import REQ_BASE_POSITION, REQ_ROTATION_IN_TIME, DominikProtocolCmds, \
    REQ_ROTATION_DEGREES, REQ_GENERAL_DATA, REQ_CLEAR_GENERAL_DATA, REQ_BREAK_STEP, REQ_CLEAR_LIST, REQ_START_TEST, \
    REQ_RESULT_PER_STEP, REQ_TEST_STATUS, RESP_GENERAL_DATA

from pydominik.volume_calculation import volume_of_steps, revolutions_of_steps
from pydominik.dominik_emulator import DominikEmulator
import json
import jsonschema


class Steps:
    def __init__(self, dominik):
        self.d = dominik

    def __enter__(self):
        self.d.reset_test()

    def __exit__(self, type, value, traceback):
        self.d.req_start_test()
        self.d.wait_for_test_to_finish()


class Dominik(dominik_net0.dominik_net0):
    def __init__(self, port_name):
        if port_name == "TEST":
            self.dominik_emulator = DominikEmulator()
            self.test_enabled = True

        else:
            self.test_enabled = False
            super().__init__(port_name)

        self.port_name = port_name
        self.estimated_time = 0

    def tester(function):
        def wrap(*args, **kwargs):
            dominik = args[0]
            dominik_emulator_args = args[1:]
            if dominik.test_enabled:
                test_function = dominik.dominik_emulator.get_tester_req_from_req(function.__name__)
                return test_function(*dominik_emulator_args, **kwargs)
            else:
                return function(*args, **kwargs)

        return wrap

    def wait_for_test_to_finish(self):
        while True:
            try:
                status_frame = self.req_test_status()
                status = status_frame.get_value("test_state")

                if status not in (2, 3):
                    raise RuntimeError("Jeszcze nie ukonczono testow")
                break
            except (RuntimeError, TimeoutError, dominik_net0.CommError):
                pass

    def steps(self, description=""):
        if len(description):
            print("Test: ", description)
        return Steps(self)

    def set_characteristics(self, filename):
        schema = {"type": "array",
                  "items":
                      {"type": "object",
                       "properties":
                           {
                               "num": {"type": "number"},
                               "direction": {"type": "string", "enum": ['p', 't']},
                               "orientation": {"type": "string", "enum": ['v', 'h']},
                               "temperature": {"type": "number"},
                               "points": {"type": "array", "items":
                                   {"type": "array",
                                    "items": [{"type": "number"},
                                              {"type": "number"}]
                                    },
                                          "maxItems": 20
                                          }
                           },
                       "required": ["num", "direction", "orientation", "temperature", "points"],
                       }
                  }
        self.characteristics = None
        with open(filename, 'r') as f:
            parsed_json = json.load(f)
            jsonschema.validate(instance=parsed_json, schema=schema)
            self.characteristics = parsed_json

    def reset_test(self):
        self.req_break_step(flag=0)
        self.req_clear_list()
        self.req_clear_general_data()

        status_frame = self.req_test_status()
        steps_count = status_frame.get_value("step_number") + 1
        status = status_frame.get_value("test_state")
        if status in (2, 3):
            for step_number in range(steps_count):
                self.req_result_per_step(step_number=step_number)

    def read_volume_and_revs(self, temperature=25.0, orientation='h'):
        if self.characteristics is None:
            raise ValueError("Brak wybranych charakterystyk")
        status_frame = self.req_test_status()
        steps_count = status_frame.get_value("step_number") + 1
        status = status_frame.get_value("test_state")

        if status not in (2, 3):
            raise RuntimeError("Jeszcze nie ukonczono testow")

        steps = []
        for step_number in range(steps_count):
            step_frame = self.req_result_per_step(step_number=step_number)
            steps.append(
                {"velocity": step_frame.get_value("velocity"),
                 "acceleration": step_frame.get_value("acceleration"),
                 "amount_of_rotation": step_frame.get_value("amount_of_rotation") / 10.0})

        revs = revolutions_of_steps(steps)
        volume = volume_of_steps(steps, temperature=temperature,
                                        orientation=orientation,
                                        characteristics=self.characteristics)
        return volume, revs

    @tester
    def req_start_test(self):
        data_frame = REQ_START_TEST()
        return self.send_cmd_net0(DominikProtocolCmds.REQ_START_TEST, data_frame)

    @tester
    def req_clear_list(self):
        data_frame = REQ_CLEAR_LIST()
        return self.send_cmd_net0(DominikProtocolCmds.REQ_CLEAR_LIST, data_frame)

    @tester
    def req_break_step(self, flag):
        data_frame = REQ_BREAK_STEP()
        data_frame.set_value("flag", flag)
        return self.send_cmd_net0(DominikProtocolCmds.REQ_BREAK_STEP, data_frame)

    @tester
    def req_rotation_in_time(self, flag, rotational_velocity, rotational_acceleration, operation_time):
        data_frame = REQ_ROTATION_IN_TIME()
        data_frame.set_value("flag", flag)
        data_frame.set_value("rotational_velocity", rotational_velocity)
        data_frame.set_value("rotational_acceleration", rotational_acceleration)
        data_frame.set_value("operation_time", operation_time)

        return self.send_cmd_net0(DominikProtocolCmds.REQ_ROTATION_IN_TIME, data_frame)

    @tester
    def req_rotation_degrees(self, flag, rotation_in_degrees, rotational_velocity, rotational_acceleration):
        data_frame = REQ_ROTATION_DEGREES()
        data_frame.set_value("flag", flag)
        data_frame.set_value("rotation_in_degrees", rotation_in_degrees)
        data_frame.set_value("rotational_velocity", rotational_velocity)
        data_frame.set_value("rotational_acceleration", rotational_acceleration)

        return self.send_cmd_net0(DominikProtocolCmds.REQ_ROTATION_DEGREES, data_frame)

    @tester
    def req_base_position(self, flag):
        data_frame = REQ_BASE_POSITION()
        data_frame.set_value("flag", flag)

        return self.send_cmd_net0(DominikProtocolCmds.REQ_BASE_POSITION, data_frame)

    @tester
    def req_result_per_step(self, step_number):
        data_frame = REQ_RESULT_PER_STEP()
        data_frame.set_value("step_number", step_number)
        return self.send_cmd_net0(DominikProtocolCmds.REQ_RESULT_PER_STEP, data_frame)

    @tester
    def req_test_status(self):
        data_frame = REQ_TEST_STATUS()
        return self.send_cmd_net0(DominikProtocolCmds.REQ_TEST_STATUS, data_frame)

    @tester
    def req_general_data(self):
        data_frame = REQ_GENERAL_DATA()
        return self.send_cmd_net0(DominikProtocolCmds.REQ_GENERAL_DATA, data_frame)

    @tester
    def req_clear_general_data(self):
        data_frame = REQ_CLEAR_GENERAL_DATA()
        return self.send_cmd_net0(DominikProtocolCmds.REQ_CLEAR_GENERAL_DATA, data_frame)
