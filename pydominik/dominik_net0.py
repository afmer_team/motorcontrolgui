import serial
import time
import struct
import logging

from pydominik import net0
from pydominik.dominik_frames import DominikProtocolCmds, DominikProtocolRsp, RESP_GENERAL_DATA, DominikDataParsers

logger = logging.getLogger(__name__)


class NackError(Exception):
    def __init__(self, message, error_code):
        super().__init__(message)
        self.error_code = error_code


class TimeoutError(Exception):
    def __init__(self, message):
        super().__init__(message)


class CrcError(Exception):
    def __init__(self, message):
        super().__init__(message)


class CommError(Exception):
    def __init__(self, message):
        super().__init__(message)


def get_status_from_byte(data):
    status = {
        0: 'FRAME_STAT_OK',
        1: 'FRAME_STAT_BUSY',
        2: 'FRAME_STAT_NOT_SUPPORTED',
        3: 'FRAME_STAT_ERR_LIMIT',
        4: 'FRAME_STAT_ERR_DATA',
        5: 'FRAME_STAT_ERR_CRC',
        6: 'FRAME_STAT_LIST_IS_CLEAR',
        7: 'FRAME_STAT_DATA_NOT_READ',
        8: 'FRAME_STAT_IS_NOT_CLEAR',
        255: 'FRAME_STAT_ERR'
    }
    try:
        return status[data]
    except KeyError:
        logging.error("Nieznany status %d".format(data))
        return None


class dominik_net0:
    def __init__(self, port_name, net=1, comm_log=False):
        self.port = serial.Serial(port=port_name, baudrate=115200,
                                  parity=serial.PARITY_NONE, writeTimeout=0)
        self.net = net
        self.comm_log = comm_log
        self.test_mode_enabled = False

    def send_cmd_net0(self, cmd, data):
        data = data.get_bytes()
        if type(cmd) is DominikProtocolCmds:
            cmd = cmd.value
        raw_frame = [cmd] + data
        packed_frame = net0.pack(raw_frame)
        self.port.write(packed_frame)
        rsp = self.wait_for_frame_net0(expected_cmd=0x80 | cmd)
        return rsp

    def _comm_log_tx(self, data):
        if self.comm_log:
            logger.info("TX -> %s", hex_string(data))

    def _comm_log_rx(self, data):
        if self.comm_log:
            print("RX -> {}".format(hex_string(data)))

    def wait_for_frame_net0(self, expected_cmd, timeout_ms=500):
        raw_frame = self._wait_for_frame_net0(timeout_ms=timeout_ms)
        unpacked_frame = net0.unpack(raw_frame)
        cmd = unpacked_frame[0]
        status = unpacked_frame[1]
        raw_data = unpacked_frame[2:]
        raw_data = bytes(raw_data)
        if expected_cmd != cmd:
            raise CommError("Odebrano cmd {} a oczekiwano {}".format(cmd, expected_cmd))
        try:
            data_frame = DominikDataParsers[DominikProtocolRsp(cmd)]()
            data_frame.parse(raw_data)
            return data_frame
        except KeyError:
            print("Brak parsera dla tego typu danych")
        return None

    def _wait_for_frame_net0(self, timeout_ms=5000):
        start_time = time.time()
        frame = bytes(0)
        while True:
            if self.port.inWaiting():
                frame += self.port.read(self.port.inWaiting())
                frame = shift_to_start(frame, net0.STX)
                if len(frame) == 0:
                    continue
                try:
                    stop_byte_index = frame.index(net0.ETX)
                    return frame
                except ValueError:
                    continue
            if time.time() - start_time > (timeout_ms / 1000.0):
                raise TimeoutError("Przekroczono {} ms oczekiwania na "
                                   "odpowiedz".format(timeout_ms))

    def __str__(self):
        return "Port = {}".format(self.port)

    def __del__(self):
        try:
            if self.port.isOpen():
                self.port.close()
        except AttributeError:
            pass


def shift_to_start(d, start):
    try:
        return d[d.rindex(start):]
    except ValueError:
        if type(d) is bytes:
            return bytes(0)
        elif type(d) is list:
            return []


def hex_string(data):
    if data:
        return ' '.join([hex(b)[2:].zfill(2).upper() for b in data])
    else:
        return "NULL"
