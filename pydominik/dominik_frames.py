from pydominik.cstruct.cstruct import Cstruct
from enum import Enum, unique


@unique
class DominikProtocolCmds(Enum):
    REQ_START_TEST = 0x01
    REQ_CLEAR_LIST = 0x02
    REQ_BREAK_STEP = 0x04
    REQ_ROTATION_IN_TIME = 0x05
    REQ_ROTATION_DEGREES = 0x06
    REQ_BASE_POSITION = 0x07
    REQ_RESULT_PER_STEP = 0x08
    REQ_TEST_STATUS = 0x09
    REQ_GENERAL_DATA = 0x0A
    REQ_CLEAR_GENERAL_DATA = 0x0B


@unique
class DominikProtocolRsp(Enum):
    RESP_GENERAL_DATA = 0x8A
    RESP_TEST_STATUS_DATA = 0x89
    RESP_RESULT_PER_STEP_DATA = 0x88
    # response only with cmd
    REQ_START_TEST = 0x81
    REQ_CLEAR_LIST = 0x82
    REQ_BREAK_STEP = 0x84
    REQ_ROTATION_IN_TIME = 0x85
    REQ_ROTATION_DEGREES = 0x86
    REQ_BASE_POSITION = 0x87
    REQ_CLEAR_GENERAL_DATA = 0x8B


# REQUESTS

class REQ_ROTATION_IN_TIME(Cstruct):
    Struct = [
        {'flag': {'type': 'uint8_t'}},
        {'rotational_velocity': {'type': 'float'}},
        {'rotational_acceleration': {'type': 'float'}},
        {'operation_time': {'type': 'uint32_t'}}
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_ROTATION_DEGREES(Cstruct):
    Struct = [
        {'flag': {'type': 'uint8_t'}},
        {'rotation_in_degrees': {'type': 'int32_t'}},
        {'rotational_velocity': {'type': 'float'}},
        {'rotational_acceleration': {'type': 'float'}}
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_BASE_POSITION(Cstruct):
    Struct = [
        {'flag': {'type': 'uint8_t'}},
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_GENERAL_DATA(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_CLEAR_GENERAL_DATA(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_START_TEST(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_CLEAR_LIST(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_BREAK_STEP(Cstruct):
    Struct = [
        {'flag': {'type': 'uint8_t'}},
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_RESULT_PER_STEP(Cstruct):
    Struct = [
        {'step_number': {'type': 'uint8_t'}},
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class REQ_TEST_STATUS(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


# RESPONSES

class RESP_GENERAL_DATA(Cstruct):
    Struct = [
        {'current_rotation_velocity': {'type': 'float'}},
        {'rotation_counter': {'type': 'int32_t'}},
        {'timestamp': {'type': 'uint32_t'}},
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_TEST_STATUS_DATA(Cstruct):
    Struct = [
        {'test_state': {'type': 'uint8_t'}},
        {'step_number': {'type': 'uint8_t'}},
        {'timestamp': {'type': 'uint32_t'}},
        {'estimated_test_time': {'type': 'uint32_t'}},
        {'current_rotational_velocity': {'type': 'float'}},
        {'rotation_counter_from_start': {'type': 'uint32_t'}},
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)
        self.parsed_data = None


class RESP_RESULT_PER_STEP_DATA(Cstruct):
    Struct = [
        {'req_type': {'type': 'uint8_t'}},
        {'velocity': {'type': 'float'}},
        {'acceleration': {'type': 'float'}},
        {'amount_of_rotation': {'type': 'int32_t'}}
    ]

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)
        self.parsed_data = None


class RESP_REQ_BREAK_STEP(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_REQ_START_TEST(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_REQ_CLEAR_LIST(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_REQ_ROTATION_IN_TIME(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_REQ_ROTATION_DEGREES(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_REQ_BASE_POSITION(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


class RESP_REQ_CLEAR_GENERAL_DATA(Cstruct):
    Struct = []

    def __init__(self):
        super(self.__class__, self).__init__(self.__class__.Struct)


DominikDataParsers = {
    DominikProtocolRsp.RESP_GENERAL_DATA: RESP_GENERAL_DATA,
    DominikProtocolRsp.RESP_RESULT_PER_STEP_DATA: RESP_RESULT_PER_STEP_DATA,
    DominikProtocolRsp.RESP_TEST_STATUS_DATA: RESP_TEST_STATUS_DATA,
    DominikProtocolRsp.REQ_START_TEST: RESP_REQ_START_TEST,
    DominikProtocolRsp.REQ_CLEAR_LIST: RESP_REQ_CLEAR_LIST,
    DominikProtocolRsp.REQ_BREAK_STEP: RESP_REQ_BREAK_STEP,
    DominikProtocolRsp.REQ_ROTATION_IN_TIME: RESP_REQ_ROTATION_IN_TIME,
    DominikProtocolRsp.REQ_ROTATION_DEGREES: RESP_REQ_ROTATION_DEGREES,
    DominikProtocolRsp.REQ_BASE_POSITION: RESP_REQ_BASE_POSITION,
    DominikProtocolRsp.REQ_CLEAR_GENERAL_DATA: RESP_REQ_CLEAR_GENERAL_DATA
}
