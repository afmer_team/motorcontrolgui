import pickle
from itertools import tee
from math import fabs

from PyQt5.QtCore import Qt, QEventLoop
from PyQt5.QtWidgets import QWidget, QLineEdit, QVBoxLayout, QHBoxLayout, \
    QLabel, QSizePolicy, QSpacerItem, QPushButton, \
    QProgressBar, QGroupBox, QFileDialog, QMessageBox, QTableWidget, QTableWidgetItem, QAbstractScrollArea, QSpinBox
from PyQt5.uic.properties import QtWidgets, QtCore

from pydominik.volume_calculation import volume_of_steps, revolutions_of_steps
from worker import Worker
import logging
import pytz
import json
import jsonschema

log = logging.getLogger(__name__)


class volume(QGroupBox):
    def __init__(self, parent):
        super(self.__class__, self).__init__("Charakterystyki")
        # data
        self.parent = parent
        self.orientation = None
        self.temperature = None
        self.direction = None
        self.amount_of_rotation = 0
        self.steps_data = []
        self.flag_test_finished = 0
        # gui
        self.read_step_button = QPushButton("ODCZYTAJ WYNIKI")
        self.table = QTableWidget(5, 2)
        self.filename_lineedit = QLineEdit("...")
        self.load_file_button = QPushButton("Załaduj")
        self.temperature_box = QSpinBox()
        self.h_button = QPushButton("POZ. POZIOMA")
        self.v_button = QPushButton("POZ. PIONOWA")
        self.t_button = QPushButton("WYBIERZ TEMPERATURĘ")
        self.set_view()
        self.set_connections()

    def set_view(self):
        main_layout = QVBoxLayout()
        file_layout = QHBoxLayout()
        config_layout = QHBoxLayout()
        read_write_layout = QHBoxLayout()
        main_layout.addWidget(self.table)
        config_layout.addWidget(self.t_button)
        config_layout.addWidget(self.temperature_box)

        read_write_layout.addWidget(self.read_step_button)
        read_write_layout.addWidget(self.h_button)
        read_write_layout.addWidget(self.v_button)

        main_layout.addLayout(read_write_layout)
        main_layout.addLayout(config_layout)

        file_layout.addWidget(self.load_file_button)
        file_layout.addWidget(self.filename_lineedit)

        self.table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        self.table.setHorizontalHeaderLabels(["WIELKOŚĆ", "WARTOŚĆ"])
        req_type = QTableWidgetItem("STAN TESTU: ")
        velocity = QTableWidgetItem("SZYBKOŚĆ:")
        acceleration = QTableWidgetItem("PRZYŚPIESZENIE:")
        amount_of_rotation = QTableWidgetItem("OBROTY:")
        volume = QTableWidgetItem("OBJĘTOŚĆ:")
        self.table.setItem(0, 0, req_type)
        self.table.setItem(1, 0, velocity)
        self.table.setItem(2, 0, acceleration)
        self.table.setItem(3, 0, amount_of_rotation)
        self.table.setItem(4, 0, volume)
        self.table.resizeColumnsToContents()
        self.table.setEnabled(False)

        transmit_layout = QHBoxLayout()
        self.filename_lineedit.setEnabled(False)

        widgets_layout = QVBoxLayout()
        widgets_layout.addLayout(file_layout)
        widgets_layout.addLayout(transmit_layout)

        main_layout.addLayout(widgets_layout)
        main_layout.addStretch(1)

        self.setLayout(main_layout)

    def set_connections(self):
        self.load_file_button.clicked.connect(self.load_file)
        self.read_step_button.clicked.connect(self.read_last_step_number)
        self.v_button.clicked.connect(self.set_orientation_vertical)
        self.h_button.clicked.connect(self.set_orientation_horizontal)
        self.t_button.clicked.connect(self.set_temperature)

    def enable_buttons(self, enable: bool):
        self.load_file_button.setEnabled(False)
        self.table.setEnabled(enable)
        self.v_button.setEnabled(enable)
        self.t_button.setEnabled(False)
        self.h_button.setEnabled(enable)
        self.temperature_box.setEnabled(False)
        self.read_step_button.setEnabled(False)
        self.table.setEnabled(False)

    @staticmethod
    def get_status_from_result(data):
        status = {
            0: 'BRAK TESTU',
            1: 'W TRAKCIE TESTU',
            2: 'TEST ZAKOŃCZONO',
            3: 'TEST ZAKOŃCZONO i ODCZYTANO',
            255: 'NIEZDEFINIOWANY STAN TESTERA'
        }
        try:
            return status[data]
        except KeyError:
            logging.error("Nieznany status %d".format(data))
            return None

    @staticmethod
    def error(self):
        log.info("Czytanie kroku nieudane")

    def load_file(self):
        file_dialog = QFileDialog(self, "Wybierz plik charakterystyki",
                                  filter="(*.json)")
        file_dialog.setFileMode(QFileDialog.ExistingFile)

        if file_dialog.exec():
            filename = file_dialog.selectedFiles()[0]
            self.filename_lineedit.setText(filename)
            log.info("Wybrano charakterystyke z {}".format(filename))
            self.parse_characteristics_file(filename)
            self.read_step_button.setEnabled(True)

    def parse_characteristics_file(self, filename):
        schema = {"type": "array",
                  "items":
                      {"type": "object",
                       "properties":
                           {
                               "num": {"type": "number"},
                               "direction": {"type": "string", "enum": ['p', 't']},
                               "orientation": {"type": "string", "enum": ['v', 'h']},
                               "temperature": {"type": "number"},
                               "points": {"type": "array", "items":
                                   {"type": "array",
                                    "items": [{"type": "number"},
                                              {"type": "number"}]
                                    },
                                          "maxItems": 20
                                          }
                           },
                       "required": ["num", "direction", "orientation", "temperature", "points"],
                       }
                  }
        self.write_characteristics = None
        self.filename_lineedit.setText("...")
        try:
            with open(filename, 'r') as f:
                try:
                    parsed_json = json.load(f)
                    jsonschema.validate(instance=parsed_json, schema=schema)
                    self.write_characteristics = parsed_json
                    self.filename_lineedit.setText(filename)
                except json.JSONDecodeError as err:
                    err_msg = "{} [{}:{}] {}".format(filename, err.lineno, err.colno, err.msg)
                    QMessageBox.critical(self, "Bład parsowania pliku charakterystyki",
                                         err_msg)
                except jsonschema.exceptions.ValidationError as err:
                    err_msg = "{}\n{}".format(err.message, err.schema)
                    QMessageBox.critical(self, "Bład formatu danych w pliku json",
                                         err_msg)

        except EnvironmentError:
            QMessageBox.critical(self, "Bład dostępu do pliku",
                                 "Nie udało się otworzyć pliku")

    def set_orientation_horizontal(self):
        self.orientation = "h"
        self.v_button.setEnabled(False)
        self.t_button.setEnabled(True)
        self.temperature_box.setEnabled(True)

    def set_orientation_vertical(self):
        self.orientation = "v"
        self.h_button.setEnabled(False)
        self.t_button.setEnabled(True)
        self.temperature_box.setEnabled(True)

    def set_temperature(self):
        self.temperature = self.temperature_box.value()
        self.load_file_button.setEnabled(True)

    def read_last_step_number(self):
        self.steps_data.clear()
        status_worker = Worker(self.parent.dominik.req_test_status)
        status_worker.signals.result.connect(self.read_all_steps)
        status_worker.signals.error.connect(self.error)
        self.parent.dominik_threadpool.start(status_worker)

    def read_all_steps(self, result):
        log.info("Zaczynam odczyt krokow")
        status = self.get_status_from_result(result.get_value("test_state"))
        self.table.setItem(0, 1, QTableWidgetItem(str(self.get_status_from_result(result.get_value("test_state")))))
        if status == 'TEST ZAKOŃCZONO':
            self.flag_test_finished = 1
        self.steps = {"done": 0, "todo": result.get_value("step_number") + 1}
        self.read_next()

    def read_next(self):
        log.info("Odczytuje krok {}/{}".format(self.steps["done"] + 1, self.steps["todo"]))
        worker = Worker(self.parent.dominik.req_result_per_step,
                        step_number=self.steps["done"])
        worker.signals.result.connect(self.steps_received)
        worker.signals.error.connect(self.error)
        self.parent.dominik_threadpool.start(worker)

    def steps_received(self, steps_frame):
        self.steps["done"] += 1
        self.steps_data.append(
            {"velocity": steps_frame.get_value("velocity"), "acceleration": steps_frame.get_value("acceleration"),
             "amount_of_rotation": steps_frame.get_value("amount_of_rotation") / 10.0})

        if self.steps["done"] < self.steps["todo"]:
            self.read_next()
        else:
            logging.info("Odczytano wszystkie kroki")
            QMessageBox.information(self, "Sukces", "Odczytano wszystkie kroki")
            self.write_values()

    def write_values(self):
        velocity = self.steps_data[self.steps["done"] - 1]["velocity"]
        acceleration = self.steps_data[self.steps["done"] - 1]["acceleration"]
        for iterate in self.steps_data:
            self.amount_of_rotation += iterate["amount_of_rotation"]

        self.table.setItem(0, 3, QTableWidgetItem(str(velocity)))
        self.table.setItem(0, 5, QTableWidgetItem(str(acceleration)))
        self.table.setItem(0, 7, QTableWidgetItem(str(revolutions_of_steps(self.steps_data))))
        if self.flag_test_finished == 1:
            self.table.setItem(0, 9, QTableWidgetItem(str(volume_of_steps(self.steps_data, temperature=self.temperature, orientation=self.orientation, characteristics=self.write_characteristics))))
        self.table.resizeColumnsToContents()

    def testing(self):
        file = open('data2', 'wb')
        pickle.dump(self.steps_data, file)
        file.close()
        steps = self.steps_data

        print("V = {} [L]".format(volume_of_steps(steps, temperature=self.temperature, orientation=self.orientation, characteristics=self.write_characteristics)))
        print("Obroty = {}".format(revolutions_of_steps(steps)))
